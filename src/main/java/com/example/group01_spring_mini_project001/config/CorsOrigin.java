package com.example.group01_spring_mini_project001.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@Configuration

    public class CorsOrigin {
        @Bean
        public CorsOrigin corsFilter(){
            final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            final CorsConfiguration config = new CorsConfiguration();
            config.setAllowCredentials(true);
            config.setAllowedOriginPatterns(Collections.singletonList("*")); // Allows all origin
            config.setAllowedHeaders(Arrays.asList("Origin","Content-Type","Accept"));
            config.setAllowedMethods(Arrays.asList("GET","POST","PUT","OPTIONS","DELETE","PATCH"));
            source.registerCorsConfiguration("/**",config);
            return new CorsOrigin();

        }
}
