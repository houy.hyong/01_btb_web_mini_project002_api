package com.example.group01_spring_mini_project001.mapper;

import com.example.group01_spring_mini_project001.dto.TaskDTO;
import com.example.group01_spring_mini_project001.model.entity.Task;
import com.example.group01_spring_mini_project001.model.request.TaskRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Mapper
public interface TaskMapper {
    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    @Mapping(source = "taskId", target = "taskId")

    List<TaskDTO> listToTaskDTO(List<Task> tasks);

    TaskDTO toTaskDto(Task task);


}
