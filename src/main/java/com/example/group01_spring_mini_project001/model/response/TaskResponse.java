package com.example.group01_spring_mini_project001.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class TaskResponse <T>{
    private T payload;
    private Timestamp date;
    private Boolean success;
}
