package com.example.group01_spring_mini_project001.controller;

import com.example.group01_spring_mini_project001.dto.TaskDTO;
import com.example.group01_spring_mini_project001.model.entity.Task;
import com.example.group01_spring_mini_project001.model.request.TaskRequest;
import com.example.group01_spring_mini_project001.model.response.TaskResponse;
import com.example.group01_spring_mini_project001.service.AuthService;
import com.example.group01_spring_mini_project001.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/tasks")
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@CrossOrigin(origins = "http://localhost:3000/")
public class TaskController {

    private final TaskService taskService;
    private final AuthService authService;


// 1- Get All Task
    @GetMapping("")
    @Operation(summary = "Get All Task")
    public ResponseEntity<TaskResponse<List<TaskDTO>>> getAllTask() {
        TaskResponse response = TaskResponse.<List<TaskDTO>>builder()
                .payload(taskService.getAllTask())
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

// 2- Get Task By Id
    @GetMapping("/{id}")
    @Operation(summary = "Get Task By Id")
    public ResponseEntity<TaskResponse<TaskDTO>> getTaskById(@RequestParam("id") Integer id) {
        TaskResponse response = TaskResponse.<TaskDTO>builder()
                .payload(taskService.getTaskById(id))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

// 3- Delete Current Task By Id
    @DeleteMapping("/{id}/users")
    @Operation(summary = "Delete Current Task By Id for current user")
    public ResponseEntity<TaskResponse<String>> deleteTaskById(@PathVariable("id") Integer taskId) {

        String email;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);

        if (taskService.deleteTaskById(taskId,userId) == true) {
            TaskResponse response = TaskResponse.<String>builder()
                    .payload("Delete this Task ID : " + taskId + " is successful")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

// 4- Add New Task
    @PostMapping("/users")
    @Operation(summary = "Add new task")
    public ResponseEntity<TaskResponse<Task>> addNewTask(@RequestBody TaskRequest taskRequest) {
        String email;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
             email = ((UserDetails)principal).getUsername();
        } else {
             email = principal.toString();
        }
        System.out.println(email);
       Integer userId =  authService.getUserIdByEmail(email);

        Task task =  taskService.addTask(taskRequest,userId);
        Integer storeId = task.getTaskId();
        if (storeId != null) {
            TaskResponse response = TaskResponse.<TaskDTO>builder()
                    .payload(taskService.getTaskById(storeId))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


// 5- Update Task Current User
    @PutMapping("/{id}/users")
    @Operation(summary = "Update task by ID for current user")
    public ResponseEntity<TaskResponse<Task>> updateTask(@RequestBody TaskRequest taskRequest, @PathVariable("id") Integer id) {
        String email;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId =  authService.getUserIdByEmail(email);
        Task task = taskService.updateTask(taskRequest,id,userId);
        Integer storeId = task.getTaskId();
        if (storeId != null) {
            TaskResponse response = TaskResponse.<TaskDTO>builder()
                    .payload(taskService.getTaskById(storeId))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }



// 6- Get Task By Id for Current User
    @GetMapping("/{id}/users")
    @Operation(summary = "Get Task By Id for Current User")
    public ResponseEntity<TaskResponse<TaskDTO>> getTaskCurrentUser(@PathVariable("id") Integer id) {
        String email;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails) principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId = authService.getUserIdByEmail(email);

        Task task = taskService.getTaskCurrentUser(id, userId);
        Integer storeId = task.getTaskId();
        if (storeId != null) {
            TaskResponse response = TaskResponse.<TaskDTO>builder()
                    .payload(taskService.getTaskById(storeId))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }


// 7- Get All Tasks for Current User
    @GetMapping("/users")
    @Operation(summary = "Get all tasks for current user")
    public ResponseEntity<TaskResponse<List<TaskDTO>>>getPageTask(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "false") Boolean asc,
            @RequestParam(defaultValue = "false") Boolean desc
            ){

        String email;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails) principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId = authService.getUserIdByEmail(email);

        TaskResponse response = TaskResponse.<List<TaskDTO>>builder()
                .payload(taskService.getPageTask(page, size,asc,desc,userId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }



// 8- Filter Task By Status for Current User
    @GetMapping("/status/users")
    @Operation(summary = "Filter task by status for current user")
    public ResponseEntity<TaskResponse<TaskDTO>> filterStatus(@RequestParam(value = "status",defaultValue = "Done") String status){
        String email;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails) principal).getUsername();
        } else {
            email = principal.toString();
        }
        System.out.println(email);
        Integer userId = authService.getUserIdByEmail(email);


        TaskResponse response = TaskResponse.<List<TaskDTO>>builder()
                .payload(taskService.filterStatus(status,userId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

}


