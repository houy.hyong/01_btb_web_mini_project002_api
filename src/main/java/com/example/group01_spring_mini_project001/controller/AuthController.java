package com.example.group01_spring_mini_project001.controller;

import com.example.group01_spring_mini_project001.model.entity.AppUser;
import com.example.group01_spring_mini_project001.model.entity.Auth;
import com.example.group01_spring_mini_project001.model.entity.LoginUser;
import com.example.group01_spring_mini_project001.model.request.AuthAutheticationRequest;
import com.example.group01_spring_mini_project001.model.response.AuthAuthenticationResponse;
import com.example.group01_spring_mini_project001.model.response.AuthRegisterResponse;
import com.example.group01_spring_mini_project001.service.AuthService;
import com.example.group01_spring_mini_project001.service.AuthenticationService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "http://localhost:3000/")
public class AuthController {

    private final AuthenticationService authenticationService;
    private final AuthService authService;


    public AuthController(AuthenticationService authenticationService, AuthService authService) {
        this.authenticationService = authenticationService;
        this.authService = authService;
    }


    @PostMapping("/authentication")
    public ResponseEntity<AuthAuthenticationResponse<LoginUser>> authenticate(
            @RequestBody AuthAutheticationRequest autheticationRequest
            ){
        AuthAuthenticationResponse<LoginUser> response = AuthAuthenticationResponse.<LoginUser>builder()
                .payload(authenticationService.authenticate(autheticationRequest))
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus(true)
                .build();
        return ResponseEntity.ok(response);
    }


    @PostMapping("/register")
    public ResponseEntity<AuthRegisterResponse<AppUser>> register(@Valid @RequestBody AuthAutheticationRequest authAutheticationRequest){
        Integer storeUserId =  authService.addNewUser(authAutheticationRequest);
        if(storeUserId != null){
            AuthRegisterResponse<AppUser> response = AuthRegisterResponse.<AppUser>builder()
                    .payload(authService.getUserById(storeUserId))
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

}
