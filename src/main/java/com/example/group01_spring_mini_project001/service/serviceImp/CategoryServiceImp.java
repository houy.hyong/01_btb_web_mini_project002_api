package com.example.group01_spring_mini_project001.service.serviceImp;

import com.example.group01_spring_mini_project001.exception.EmptyFieldOrInvalidException;
import com.example.group01_spring_mini_project001.exception.NotOwnerException;
import com.example.group01_spring_mini_project001.exception.NotFoundException;
import com.example.group01_spring_mini_project001.model.entity.Category;
import com.example.group01_spring_mini_project001.model.request.CategoryRequest;
import com.example.group01_spring_mini_project001.repository.CategoryRepository;

import com.example.group01_spring_mini_project001.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private final CategoryRepository categoryRepository;
    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    // 1 - Get all categories
    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.findAllCategory();
    }


    // 2 - Get category by ID
    @Override
    public Category getCategoryById(Integer categoryId) {
        if(categoryRepository.getCategoryById(categoryId)!= null){
            return categoryRepository.getCategoryById(categoryId);
        }else{
            throw new NotFoundException("Category ID : "+ categoryId +" not found");
        }

    }


    // 3 - Add new category
    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest, Integer userId) {

        if(categoryRequest.getCategoryName().isBlank()){
            throw new EmptyFieldOrInvalidException("CategoryName must not be empty !!!");
        }else{
            Integer storeCategoryId = categoryRepository.addNewCategory(categoryRequest,userId);
            return storeCategoryId;
        }



    }


    // 4 - Update Category by ID
    @Override
    public Category updateCategoryById(CategoryRequest categoryRequest, Integer categoryId, Integer userId) {

        Category category = categoryRepository.getCategoryById(categoryId);

        if(category !=null){
            if(categoryRequest.getCategoryName().isBlank()){
                throw new EmptyFieldOrInvalidException("CategoryName must not be empty !!!");
            }else{

                Integer storeCategoryId = category.getUserId();

                if (storeCategoryId == userId){
                    if (categoryRequest.getCategoryName().isEmpty()){
                        throw new EmptyFieldOrInvalidException("categoryName could not be empty");
                    }
                    else {
                        return categoryRepository.updateCategory(categoryRequest, categoryId);
                    }
                }
                else{
                    throw new NotOwnerException("You cannot update Category ID : " + categoryId + " because you're not owner");
                }
            }

        }else {
            throw new NotFoundException("Category ID : "+categoryId+ " cannot update, because it's not exist");
        }

    }



    // 5 - get Category by ID for current user
    @Override
    public Category getCurrentUserCategoryById(Integer categoryId, Integer userId) {

        Category category = categoryRepository.getCategoryById(categoryId);

        if(category != null){
            Integer storeUserId = category.getUserId();

            if (storeUserId == userId){
                if (categoryRepository.getCurrentUserCategoryById(categoryId, userId) != null){
                    return categoryRepository.getCurrentUserCategoryById(categoryId, userId);
                }
                else {
                    throw new NotFoundException("Category ID : " +categoryId+ " doesn't exist!!");
                }
            }
            else throw new NotOwnerException("You're not the owner of Category ID : " + categoryId);
        }else{
            throw new NotFoundException("Category ID : "+ categoryId + " not found");
        }

    }



    // 6 - get all Categories for current user
    @Override
    public List<Category> getCurrentUserAllCategories(Integer userId, Integer page, Integer size, Boolean asc, Boolean desc) {
        List<Category> categories = categoryRepository.getAllCategoriesForCurrentUser(userId,page,size,asc,desc);




        if (categories.size() > 0 ){
           return categories;
       }
       else
           throw new NotFoundException("No Data");
    }


    // 7 - delete category by id for current user
    @Override
    public boolean deleteCategoryById(Integer categoryId, Integer userId) {
        Category category = categoryRepository.getCategoryById(categoryId);

        if(category!=null){

            Integer storeUserId = category.getUserId();

            if (storeUserId == userId){
                if (categoryRepository.deleteCategoryById(categoryId) == true){
                    return true;
                }
            }
            else {
                throw new NotOwnerException("Category ID : " + categoryId + " cannot delete, because you're not owner !!");
            }

            return false;
        }else {
            throw new NotFoundException("Category ID : " + categoryId + " not found");
        }

    }
}
