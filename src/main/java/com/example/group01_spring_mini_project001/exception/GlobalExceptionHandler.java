package com.example.group01_spring_mini_project001.exception;

import org.springframework.http.*;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ProblemDetail;
import org.springframework.http.converter.json.ProblemDetailJacksonMixin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.time.Instant;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({AuthNotFoundException.class})
    ProblemDetail handleAuthNotFoundException(AuthNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        problemDetail.setTitle("User Not Found Exception");
        problemDetail.setDetail("User Not Found");
        problemDetail.setProperty("timestamp",Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/user/not-found"));
        return  problemDetail;
    }

    @ExceptionHandler(InvalidException.class)
    ProblemDetail  handlerFileInputNotFoundException(InvalidException e){
        ProblemDetail  problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());
        problemDetail.setTitle(" User Can't Register");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/books/bad-request"));
        return problemDetail;
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ProblemDetail handle(MethodArgumentNotValidException e) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getFieldError().getField());
        problemDetail.setTitle(" User Can't Register");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/books/bad-request"));
        return problemDetail;
    }


    @ExceptionHandler(NotFoundException.class)
    ProblemDetail TaskNotFound(NotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND, e.getMessage()
        );
        problemDetail.setTitle("Not Found");
        problemDetail.setType(URI.create(("localhost:8080/success/not-found")));
        return problemDetail;
    }

    @ExceptionHandler(EmptyFieldOrInvalidException.class)
    ProblemDetail emptyFieldOrInvalidHandler(EmptyFieldOrInvalidException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST, e.getMessage()
        );

        problemDetail.setTitle("Invalid Field");
        problemDetail.setType(URI.create("localhost:8080/task/error/bad-request"));
        return problemDetail;
    }

    @ExceptionHandler(NotOwnerException.class)
    ProblemDetail NotOwner(NotOwnerException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.FORBIDDEN, e.getMessage()
        );
        problemDetail.setTitle("You're not owner!!!");
        problemDetail.setType(URI.create(("localhost:8080/success/forbidden")));
        return problemDetail;
    }


}
