package com.example.group01_spring_mini_project001.exception;

public class NotOwnerException extends RuntimeException{
    public NotOwnerException(String message){
        super(message);
    }
}
