package com.example.group01_spring_mini_project001.exception;

public class InvalidException extends RuntimeException{
    public InvalidException(String message){
        super(message);
    }
}
