package com.example.group01_spring_mini_project001.repository;

import com.example.group01_spring_mini_project001.model.entity.AppUser;
import com.example.group01_spring_mini_project001.model.entity.Auth;
import com.example.group01_spring_mini_project001.model.request.AuthAutheticationRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AuthRepository {

    @Select("INSERT INTO  user_tb(email,password) " +
            "VALUES (#{request.email}, #{request.password}) " +
            "RETURNING id")
    Integer addNewUser(@Param("request") AuthAutheticationRequest authAutheticationRequest);


    @Select("SELECT * FROM  user_tb  WHERE  email = #{info}")
    Auth getUserByEmail(@Param("info") String email);


    @Select("SELECT * FROM user_tb WHERE id = #{userId}")
    AppUser getUserById(Integer userId);


    @Select("SELECT id FROM user_tb WHERE email = #{email}")
    Integer getUserIdByEmail(String email);

}
